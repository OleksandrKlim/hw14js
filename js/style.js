"use strict";
let banner = document.querySelector(".banner");
let textList = document.querySelectorAll(".portfolio-text");
window.onload = function () {
  if (localStorage.getItem("background") !== null) {
    banner.classList = localStorage.getItem("background");
    for (const i of textList) {
      i.classList = localStorage.getItem("textStyle");
    }
  }
};
let button = document.querySelector(".button-text");
button.addEventListener("click", (e) => {
  e.preventDefault();
  banner.classList.toggle("banner-lite");
  localStorage.setItem("background", banner.classList);
  for (const i of textList) {
    i.classList.toggle("dark-text");
  }
  localStorage.setItem("textStyle", textList[0].classList);
  console.log(textList[0].classList);
});
